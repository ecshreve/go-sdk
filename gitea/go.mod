module code.gitea.io/sdk/gitea

go 1.13

require (
	github.com/go-fed/httpsig v1.1.0
	github.com/hashicorp/go-version v1.5.0
	github.com/stretchr/testify v1.7.0
	golang.org/x/crypto v0.0.0-20220525230936-793ad666bf5e
)
